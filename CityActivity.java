package com.example.uapv1900102.tp3;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author HEMAIZI Camilia
 */
public class CityActivity extends AppCompatActivity {
    private ProgressDialog pDialog;

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    WeatherDbHelper dbHelper;
    JSONResponseHandler json;
    InputStream is;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        dbHelper = new WeatherDbHelper(this);

        /*
         *récuperer l'objet City envoyer par MainActivity grace a la méthode getParcelableExtra
         */
        city = (City) getIntent().getParcelableExtra("city");

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

       updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                findWeather(v);



            }
        });

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }


    /*
     * Permet dafficher les données dans CityActivity
     */
    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getCloudiness()+" %");
        textLastUpdate.setText(city.getLastUpdate());

       if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
          imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName()), getApplicationContext().getTheme()));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }
    }

    public void findWeather(View v) {
        try {
             URL url = WebServiceUrl.build(city.getName(),city.getCountry());
             ExecuteTask task = new ExecuteTask();
             task.execute(url.toString());
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }

/**
 * Implementation de la class ExecuteTask afin de
 * redefinir les méthode doInBackground et onPostExecute de la méthode AsyncTask
 */
    public class ExecuteTask extends AsyncTask<String, Void, String> {

    /**
     * Before starting background thread Show Progress Dialog
     * */
    boolean failure = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CityActivity.this);
            pDialog.setMessage("Mis a jour..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            String result= "";
            URL url;
            HttpURLConnection urlConnection=null;

            try
            {
                url = new URL(strings[0]);
                urlConnection= (HttpURLConnection) url.openConnection();
                 is = urlConnection.getInputStream();

                json = new JSONResponseHandler(city);
                json.readJsonStream(is);

                return result;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }


            return null;

        }

    /**
     * Recuperer le fichier Json a partir de l'API
     */
        @Override
        protected void onPostExecute(String s){
            super.onPostExecute(s);

            try {
                int res=  dbHelper.updateCity(city);

               updateView();
                /*
                 * afficher un message pour informer que la requete a etait executer avec succes
                 */
                AlertDialog alertDialog = new AlertDialog.Builder(CityActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(" Mise a jour enregistré  !")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                        .show();

                pDialog.dismiss();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( pDialog!=null && pDialog.isShowing() ){
            pDialog.cancel();
        }
    }
}
